[Home](../README.md)

# Table of Contents

개발 환경 구성
1. [Equipment](#environment)
2. [Environment](#equipment)


# 개발 환경 구성

## Equipment
1. IBM Training Room PC (Windows)
1. 혹은 인터넷 접속이 되는 개인 PC
1. GitLab 계정


## Environment

원활한 작업을 위하여 아래 툴들이 미리 설치된 컨테이너 환경을 Provision하여 사용합니다. 

> ### 로컬 환경에서 작업할 경우
> 로컬 머신(Windows 혹은 임의의 Laptop OS)을 사용하는 경우 아래 툴들을 직접 설치하여야 합니다.
>
> | Name	| Version 	| 필요 커맨드 |
> |---	|---	    |---	|
> | Git   | 2.30 이상  | git  |
> | Java	| SE 20 이상 | java	|
> | Maven	| 3.9 이상   | mvn	|
> | Node	| 18 이상 	 | node	|


### Step 1. GitLab 로그인
[GitLab](https://about.gitlab.com/)에 로그인하여 계정을 확인하거나, 새 계정을 생성합니다.

> 교육 진행자에게 본인의 계정을 알려주세요. 교육 진행을 위한 Gitlab Group에 초대해 드리겠습니다. 


### Step 2. 실습 Repository fork
[실습 레포지토리](https://gitlab.com/client-engineering-korea/isw-enablement)에 접속하여 상단의 **Fork** 버튼을 선택합니다.
**Fork project** 버튼을 눌러 내 계정의 레포지토리로 fork 합니다.


### Step 3. Gitpod에 로그인
[Gitpod](https://www.gitpod.io/)에 접속하여 우측 상단 **Login** 버튼을 누릅니다.
이 후, **Continue with GitLab** 버튼을 클릭하여 화면의 안내에 따라 계정을 설정합니다.

> 기본 Editor를 선택하는 화면이 나올 경우 VS Code Browser 옵션을 선택하여 주세요.

### Step 4. Gitpod Workspace 만들기
[새로 진입한 화면](https://gitpod.io/workspaces)에서 **New Workspace** 버튼을 클릭합니다. 

1. Select a repository: Step 2에서 fork한 레포지토리를 선택합니다.
1. VS Code: 기본 설정 (Browser)를 사용합니다.
1. Standard: Large 설정 (8 cores, 16GB RAM)을 사용합니다.

**Continue** 버튼을 클릭 후 잠시 기다리면 새로 provision된 workspace로 진입합니다. 새 창이 열리지 않을 경우 [Workspace](https://gitpod.io/workspaces) 화면에서 방금 생성한 환경을 클릭하여주세요. 

> 첫 사용 시, 휴대폰을 통한 본인 인증이 요구될 수 있습니다. 

> Gitpod는 미리 정의된 컨테이너 환경에 나의 레포지토리 소스 코드를 파일시스템으로 마운트하여 Web IDE (VS Code)를 통해 사용할 수 있는 운영 환경(Workspace)를 제공합니다. 각각의 운영 환경은 하나의 쿠버네티스 Pod 입니다. 


### Step 5. 필요한 툴 확인하기
웹 IDE에서 터미널을 열고 *(더보기 버튼 > View > Terminal)* 아래 명령어를 각각 입력하여 필요한 툴을 확인하세요. 

```
which git
which java
which mvn
which node
which npm
```

이렇게 생성한 Gitpod 인스턴스가 저희의 개발 환경이됩니다. `/workspace` 이외 영역의 변경 사항은 저장되지 않으니 시스템에 대한 변경 (apt install, npm install --global)을 하실 때 염두에 두시길 바랍니다. 
