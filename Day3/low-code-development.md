[Home](../README.md)


# Table of Contents
- [Preparation](#preparation)
- [Servicing Order for Cash withrawal Sequence Diagram](#servicing-order-for-cash-withrawal-sequence-diagram)
- [Development](#development)

---

## Preparation  

- Gitlab update fork
 gitlab에서 update fork 클릭!
 ![updatefork](../Day2/images/updatefork.png)

- cli 설정  

  ``` 
  npx k5 setup --file cli-config.json
  ```

### API 로직 개발  
API Provider는 Service를 호출 하여 해당 endpoint에 맞는 동작을 지원함  

![api](images/api-operation.png)


1. Autowired for Service and entity Builder
 - 각 클래스에서 사용하는 서비스는(Annotation을 호출하기 위해서는) @Autowired를 이용하여 부른다. Annotation으로 이루어진 class는 인스턴스를 하나만 생성하여 쓰기 때문에
 생성된 것을 가져다 쓰겠다는 의미 임.

```
  @Autowired
  WithdrawalDomainService withdrawaDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;
````  


2. WithdrawalDomainService를 호출 하여 요청이 왔을 때 현금인출을 요청하기 위한 로직 작성
- Service를 호출할 때 넘겨주는 Input Parameter를 생성
  이 때 넘겨주는 Input Parameter(WithdrawalDomainServiceInput)는 도메인 layer에 정의 되어 있고  
  도메인 layer에 정의된 entity들은 Domain Entity Builder 를 이용하여 만들어 준다.  

  ```
    WithdrawalDomainServiceInput withdrawalInput = entityBuilder.getSvcord()
    .getWithdrawalDomainServiceInput()
    .setAccountNumber(withdrawalBodySchema.getAccountNumber())
    .setAmount(withdrawalBodySchema.getAmount())
    .build();
  ```
- WithdrawalDomainService 호출
  Solution Designer에서 Service로 설계한 것은 *execute()* 함수에 의해 실행된다.  
  ```
  WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = withdrawaDomainService.execute(withdrawalInput);
  ``` 
     
- Service로부터 받은 값을 갖고 API에 대한 Response 작성
  Error발생시 ErrorSchema로, 정상 동작 시 WithdrawalResponse로 전달 한다.  
  ```
    if(withdrawalDomainServiceOutput == null){
    ErrorSchema errorSchema = new ErrorSchema();
    errorSchema.setErrorId("500");
    errorSchema.setErrorMsg("INTERNAL ERROR");
    ResponseEntity.status(500).body(errorSchema);
  }

  WithdrawalResponseSchema withdrawalResponse = new WithdrawalResponseSchema();

  withdrawalResponse.setServicingOrderWorkTaskResult(withdrawalDomainServiceOutput.getServicingOrderWorkResult().toString());
  withdrawalResponse.setTransactionID(withdrawalDomainServiceOutput.getTrasactionId());

  return ResponseEntity.status(200).body(withdrawalResponse);
  ```


### Service 로직 개발
API layer로 부터 호출 되어, 해당 endpoint에 대한 실제 동작을 정의 함

![service](images/service.png)

1. Autowired for command, integration services and Integration Entity Builder
  ```
    @Autowired
    Svcord19Command servicingOrderProcedureCommand;

    @Autowired
    PaymentOrder paymentOrder;

    @Autowired
    RetrieveLoginService retrieveLogin;

    @Autowired
    IntegrationEntityBuilder integrationEntityBuilder;
  ```
2. 로직 구성 
  - 사용자 인증요청 Party Life Cycle Management 호출  
  ```
    RetrieveLoginServiceInput retrieveInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginServiceInput()
    .setId("test1")
    .build();

    RetrieveLoginServiceOutput retriveSerivceOutput = retrieveLogin.execute(retrieveInput);
  ```
  - User Valid 체크
  ```
      if(retriveSerivceOutput.getResult() != "SUCCESS") {
      return null;
    }

  ```

  - Servicing Order가 시작됨을 DB에 저장. 이때 status = "PROCESSING"- command에서 설정
  ```
    CustomerReferenceEntity customerReference = this.entityBuilder
    .getSvcord()
    .getCustomerReference().build();

    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvcord()
    .getCreateServicingOrderProducerInput().build();

    createInput.setCustomerReference(customerReference);
    Svcord19 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);
  ```
  - 현금 출금 요청을 위한  Payment Order 호출
  ```
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD19");
    paymentOrderInput.setExternalSerive("SVCORD19");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);
  ```
  - Payment order결과를 DB에 업데이트 하고 해당 아웃풋을 전달
  ```
    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();

    return withdrawalDomainServiceOutput;
  ```

### Command  로직 개발
DB에 저장되어 잇는 데이터의 Creation,  update, delete의 방법을 정의한다.  
JPA가 기본적으로 설정되어 있어 ORM 을 사용 가능하게 란다. 여기에서는 save를 통해 creation과 update를, delete를 통해 삭제.
 - ISW에서 생성하는 repo를 이용하여 사용 가능

 1. Create Control Record
 ```
    Svcord19Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord19().build();
    servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
    servicingOrderProcedure.setProcessStartDate(LocalDate.now());
    servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
    servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
    servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
    servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
    
    ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
    thirdPartyReference.setId("test1");
    thirdPartyReference.setPassword("password");
    servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

    return repo.getSvcord().getSvcord19().save(servicingOrderProcedure);
 ```

 2. Update Control Record
```
  Svcord19Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord19().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
  servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
  servicingOrderProcedure.setProcessEndDate(LocalDate.now());
  servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
  servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

  log.info(updateServicingOrderProducerInput.getUpdateID().toString());
  log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
  log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

  this.repo.getSvcord().getSvcord19().save(servicingOrderProcedure);
```

### Integration 구성

   전달 받은 데이터를 이용하여 다른 서비스를 호출 하고, 받은 결과를 Service로 전달 합.   
   namespace별 directory, endpoint 별 class 생성.  
   Party lifecycle Management에서는 사용자에 대한 인증 결과를  
   Payment Order 에서는 payment order를 생성하기 위한 요청 및 결과를 전달 하게 됨.  
  ![partyservice](images/partyservice.png)
  ![payorderservice](images/paymentorderservice.png)

1. Party Life Cycle Management에서 User확인
```
  HttpHeaders httpHeaders = new HttpHeaders();
  httpHeaders.set("accept", "application/json");
  httpHeaders.set("Content-Type", "application/json");  

  LoginResponse loginResponse = partyLife.getUserLogin(retrieveLoginServiceInput.getId(), httpHeaders).getBody();

  RetrieveLoginServiceOutput loginOuput = this.entityBuilder.getPartylife().getRetrieveLoginServiceOutput().build();
  loginOuput.setResult(loginResponse.getResult().getValue());

  return loginOuput;
```  

2. payment Order 요청
- Autowired for Payament Order API 호출하기 위한 Provider
  ```
    @Autowired
    PaymentOrderApiPaymord paymentOrderAPI;
  ```
- Http header 구성
  ```
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");
  ```
- Request Body 만들기
  ```  
    CreatePaymentOrderRequest createPaymentOrderRequest = new CreatePaymentOrderRequest();
    CusotmerReference customerReference = new CusotmerReference();
    customerReference.setAccountNumber(paymentOrderInput.getAccountNumber());
    customerReference.setAmount(paymentOrderInput.getAmount());
    PaymentInitiatorSchema paymentInitiator = new PaymentInitiatorSchema();
    paymentInitiator.setExternalID(paymentOrderInput.getExternalId());
    paymentInitiator.setExternalService(paymentOrderInput.getExternalSerive());

    createPaymentOrderRequest.setCustomerReference(customerReference);
    createPaymentOrderRequest.setPaymentInitiator(paymentInitiator);
    createPaymentOrderRequest.setPaymentType(PaymentTypeEnum.CASH_WITHDRAWAL);
  ```
- Payment Order 요청 후 response 내용으로 return하기
  ```
    CreatePaymentOrderResponse paymentOrderResponse = paymentOrderAPI.createPaymentOrder(createPaymentOrderRequest, httpHeaders).getBody();
    
    PaymentOrderOutput paymentOrderOutput = this.entityBuilder.getPaymord().getPaymentOrderOutput().build();
    paymentOrderOutput.setPaymentOrderReulst(paymentOrderResponse.getContents().getPaymentOrderResult());
    paymentOrderOutput.setTransactionId(paymentOrderOutput.getTransactionId());
    return paymentOrderOutput;
  ```

### 추가 환경 설정

1. DataResource 설정
  ISW에서는 RDBMS에 대한 configuration은 자동으로 설정 되지 않음. 
  JPA에서 사용하는 data resource는 filtered/application.yaml에 정의 함.

  ```
  spring.datasource:
  url: "jdbc:db2://${DB_URL}:sslConnection=false;currentSchema=IBEE;"
  username: ${DB_USER}
  password: ${DB_PASSWORD}
  driver-class-name: com.ibm.db2.jcc.DB2Driver
  ```
  처음 delpoy시  ddl-auto는 create 설정. 이후 변경 사항이 있을 경우 update로 변경 할 것.

  ```
  spring.jpa:
  hibernate:
    ddl-auto: create
  ```  
2. ConfigMap혹은 Secret사용을 위한 extension-values.yaml설정
```
env:
  variables:
    secretKeyRef:
      - variableName: DB_URL
        secretName: k5-db-credential
        secretKey: url
        optional: false
      - variableName: DB_PASSWORD
        secretName: k5-db-credential
        secretKey: password
        optional: false
      - variableName: DB_USER
        secretName: k5-db-credential
        secretKey: id
        optional: false
```

## Ready to Deploy
- compile
```
npx k5 compile
```

- mvn 
```
mvn clean install
```

- gitlab에 로직이 반영된 코드 반영
```
npx k5 push -m "commit comment" 
```




  

